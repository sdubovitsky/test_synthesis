from django.shortcuts import render
from hotel.models import Hotel, Room

from django.db.models import Count


def hotel_list(request):
    min_room_count = request.GET.get('min_room_count')
    if min_room_count:
        hotel_ids = Room.objects.values('hotel_id').annotate(cnt=Count('*')).\
            filter(cnt__gte=min_room_count).values('hotel_id')
        hotels = Hotel.objects.filter(id__in=hotel_ids)
    else:
        hotels = Hotel.objects.all()
    return render(request, 'hotel/list.html', {'hotels': hotels})
