from django.db import models


class Hotel(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'


class Room(models.Model):
    room_id = models.IntegerField()
    hotel = models.ForeignKey(Hotel, on_delete='CASCADE')
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Стоимость')

    def __str__(self):
        return str(self.room_id)

    class Meta:
        verbose_name = 'Комната'
        verbose_name_plural = 'Комнаты'
