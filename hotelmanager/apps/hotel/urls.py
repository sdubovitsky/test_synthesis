from django.conf.urls import url

from hotel.views import hotel_list

urlpatterns = [
    url(r'^list/$', view=hotel_list, name='hotel_list'),
]
