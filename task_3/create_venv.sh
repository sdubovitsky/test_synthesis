#!/usr/bin/env bash

pip install virtualenv
virtualenv -p python3 ~/venvs/test-synthesis
source ~/venvs/test-synthesis/bin/activate
pip install -r requirements.txt
