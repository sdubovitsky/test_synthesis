import logging.config
import requests

from datetime import datetime

from typing import List, Tuple
from bs4 import BeautifulSoup


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': u'%(asctime)s:%(levelname)s:%(name)s.%(funcName)s: %(message)s',
        },
    },
    'handlers': {
        'basic': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'root': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'cruise_finder': {
            'handlers': ['basic'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}


logging.config.dictConfig(LOGGING)
logger = logging.getLogger('cruise_finder')


class CruiseFinder:

    SITE_URL = 'https://www.lueftner-cruises.com'
    RIVER_CRUISES_URI = '/en/river-cruises/cruise.html'

    HEADERS = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/70.0.3538.110 Safari/537.36',
    }

    def __init__(self, limit: int = 4) -> None:
        self.limit = limit

    def _get_html(self, uri: str) -> str:
        url = self.SITE_URL + uri
        logger.debug(f'Get html from url: {url}')
        resp = requests.get(url, headers=self.HEADERS)
        logger.debug(f'Got status: {resp.status_code}')
        return resp.text

    @staticmethod
    def _get_soup(html: str) -> BeautifulSoup:
        logger.debug('Soup cooking')
        soup = BeautifulSoup(html, 'html5lib')
        return soup

    @staticmethod
    def _parse_price(price: BeautifulSoup) -> Tuple[str, str, float]:
        date = price.find('span', {'class': 'price-duration'}).text.split('-')[0].strip()
        date = datetime.strptime(date, '%d. %b %Y').strftime('%Y-%m-%d')

        ship = price.find('span', {'class': 'fakelink'}).text

        cost = price.find('span', {'class': 'big-table-font'}).text.strip().split(' ')[1]
        cost = float(cost.replace('.', '').replace(',', '.'))

        return date, ship, cost

    def _parse_cruise(self, cruise_uri: str) -> dict:

        cruise_html = self._get_html(cruise_uri)
        cruise_soup = self._get_soup(cruise_html)

        name = cruise_soup.find('div', {'class': 'river-site-highlight'}). \
            find('h1', recursive=False).text.split('\n')[0]

        days = cruise_soup.find('p', {'class': 'cruise-duration pull-right'}).text.split()[0]

        itinerary = [''.join(r.text.split()) for r in cruise_soup.find_all('span', {'class': 'route-city'})]

        prices = cruise_soup.find('div', {'class': 'accordeon-data-price'}). \
            findAll('a', {'class': 'collapsed panel-title'})

        dates = {}
        for p in prices:
            date, ship, cost = self._parse_price(p)
            dates[date] = {
                'ship': ship,
                'price': cost
            }

        return {
            'name': name,
            'days': int(days),
            'itinerary': itinerary,
            'dates': dates
        }

    def _parse(self) -> List[dict]:
        cruises_data = []
        html = self._get_html(self.RIVER_CRUISES_URI)
        soup = self._get_soup(html)

        cruise_uris = [c.attrs['href'] for c in soup.findAll('a', {'class': 'visible-xs-block'}, limit=self.limit)]

        for uri in cruise_uris:
            d = self._parse_cruise(uri)
            cruises_data.append(d)

        return cruises_data

    def run(self) -> List[dict]:
        try:
            return self._parse()
        except Exception as e:
            logger.exception('Got unhandled error')


if __name__ == '__main__':
    logger.info('Start script')
    data = CruiseFinder().run()
    logger.info(f'Data: {data}')
