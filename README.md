## Задание №1
Таблица в базе данных:
create table hotelrooms(id int, room_id int, hotel_id int, price int)
Написать запрос который выведет ссылки на отель в котором количество комнат меньше 20.
##### Решение: 
```
SELECT hotel_id FROM hotel_room GROUP BY hotel_id HAVING COUNT(*) >= 1
```

## Задание №2
Для предыдущей задачи сделать django model.
И написать этот же запрос используя django orm.
##### Решение: 
```
Room.objects.values('hotel_id').annotate(cnt=Count('*')).filter(cnt__gte=min_room_count).values('hotel_id')

см. django приложение hotelmanager
```

## Задание №3
Получить данные с сайта https://www.lueftner-cruises.com/en/river-cruises/cruise.html
Данные положить в список (для примера список заполнен из первой ссылки):
```
[{
    "name": "Tulip Serenade",
    "days": 8,
    "itinerary": ["amsterdam", "amsterdam", "arnhem", ...],
    "dates":[
        {"2019-04-04": {
                "ship": "ms amadeus queen", "price": 1044.65
            },
    ...}
    ],
}, ...]
```
Достаточно первых 4-х ссылок.
При получении данных с сайта не забыть о корректном user-agent.
В этом задании нужен скрипт работающий на python 3 под Linux, и еще один скрипт который создаёт виртуальное окружение с установкой всех нужных модулей.
##### Решение: 
```
см. task_3
```


## Задание №4
В этом задании работающий код не нужен, достаточно обозначить какие библиотеки и как будут использованы, если данные из предыдущего задания будут в виде xml файла.
##### Решение: 
```
инициализировать BeautifulSoup(html, 'html5lib') с lxml вместо html5lib, предварительно установив: pip install lxml
```
